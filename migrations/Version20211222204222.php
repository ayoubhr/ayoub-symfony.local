<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211222204222 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE comentario (id INT AUTO_INCREMENT NOT NULL, usuario_id INT NOT NULL, texto LONGTEXT NOT NULL, rutina_id INT NOT NULL, date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_spanish_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mensajes (id INT AUTO_INCREMENT NOT NULL, full_name VARCHAR(255) NOT NULL, mensaje LONGTEXT NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_spanish_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plan_entreno (id INT AUTO_INCREMENT NOT NULL, imagen VARCHAR(255) NOT NULL, nombre VARCHAR(255) NOT NULL, ejercicio_uno VARCHAR(255) NOT NULL, ejercicio_dos VARCHAR(255) NOT NULL, ejercicio_tres VARCHAR(255) NOT NULL, ejercicio_cuatro VARCHAR(255) NOT NULL, ejercicio_cinco VARCHAR(255) NOT NULL, descripcion LONGTEXT NOT NULL, usuario_creador_id INT NOT NULL, tipo VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_spanish_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE suscripcion (id INT AUTO_INCREMENT NOT NULL, id_usuario INT NOT NULL, id_plan_entreno INT NOT NULL, fecha_inscripcion DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_spanish_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, nombre VARCHAR(255) NOT NULL, apellidos VARCHAR(255) NOT NULL, fecha_alta DATE DEFAULT NULL, telefono INT DEFAULT NULL, direccion VARCHAR(255) DEFAULT NULL, rol VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_spanish_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE comentario');
        $this->addSql('DROP TABLE mensajes');
        $this->addSql('DROP TABLE plan_entreno');
        $this->addSql('DROP TABLE suscripcion');
        $this->addSql('DROP TABLE usuario');
    }
}
