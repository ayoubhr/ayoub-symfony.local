<?php

namespace App\Form;

use App\Entity\PlanEntreno;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlanEntrenoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        //array('data_class' => null)
        $builder
            ->add('imagen', FileType::class)
            ->add('nombre', TextType::class)
            ->add('ejercicio_uno', TextType::class)
            ->add('ejercicio_dos', TextType::class)
            ->add('ejercicio_tres', TextType::class)
            ->add('ejercicio_cuatro', TextType::class)
            ->add('ejercicio_cinco', TextType::class)
            ->add('descripcion', TextareaType::class)
            ->add('tipo', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PlanEntreno::class,
        ]);
    }
}
