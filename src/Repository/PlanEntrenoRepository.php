<?php

namespace App\Repository;

use App\Entity\PlanEntreno;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlanEntreno|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlanEntreno|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlanEntreno[]    findAll()
 * @method PlanEntreno[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanEntrenoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlanEntreno::class);
    }

    // /**
    //  * @return PlanEntreno[] Returns an array of PlanEntreno objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlanEntreno
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
