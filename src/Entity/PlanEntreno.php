<?php

namespace App\Entity;

use App\Repository\PlanEntrenoRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PlanEntrenoRepository::class)
 * @UniqueEntity(fields={"nombre"}, message="El nombre introducido para la rutina ya existe")
 */
class PlanEntreno implements \ArrayAccess
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Por favor, introduce una imagen.")
     * @Assert\File(mimeTypes={ "image/jpeg", "image/png", "image/jpg", "image/gif" })
     * @Assert\Image(
     *     maxWidth = 400,
     *     maxHeight = 400
     * )
     */
    private $imagen;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Introduce un nombre para tu rutina")
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="introduce los cinco ejercicios que la componen")
     */
    private $ejercicio_uno;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="introduce los cinco ejercicios que la componen")
     */
    private $ejercicio_dos;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="introduce los cinco ejercicios que la componen")
     */
    private $ejercicio_tres;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="introduce los cinco ejercicios que la componen")
     */
    private $ejercicio_cuatro;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="introduce los cinco ejercicios que la componen")
     */
    private $ejercicio_cinco;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="describe tu rutina")
     */
    private $descripcion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $usuarioCreador_id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="indica la especialidad")
     */
    private $tipo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImagen(): ?string
    {
        return $this->imagen;
    }

    public function setImagen(string $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getEjercicioUno(): ?string
    {
        return $this->ejercicio_uno;
    }

    public function setEjercicioUno(string $ejercicio_uno): self
    {
        $this->ejercicio_uno = $ejercicio_uno;

        return $this;
    }

    public function getEjercicioDos(): ?string
    {
        return $this->ejercicio_dos;
    }

    public function setEjercicioDos(string $ejercicio_dos): self
    {
        $this->ejercicio_dos = $ejercicio_dos;

        return $this;
    }

    public function getEjercicioTres(): ?string
    {
        return $this->ejercicio_tres;
    }

    public function setEjercicioTres(string $ejercicio_tres): self
    {
        $this->ejercicio_tres = $ejercicio_tres;

        return $this;
    }

    public function getEjercicioCuatro(): ?string
    {
        return $this->ejercicio_cuatro;
    }

    public function setEjercicioCuatro(string $ejercicio_cuatro): self
    {
        $this->ejercicio_cuatro = $ejercicio_cuatro;

        return $this;
    }

    public function getEjercicioCinco(): ?string
    {
        return $this->ejercicio_cinco;
    }

    public function setEjercicioCinco(string $ejercicio_cinco): self
    {
        $this->ejercicio_cinco = $ejercicio_cinco;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getUsuarioCreadorId(): ?int
    {
        return $this->usuarioCreador_id;
    }

    public function setUsuarioCreadorId(int $usuarioCreador_id): self
    {
        $this->usuarioCreador_id = $usuarioCreador_id;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function offsetExists($offset)
    {
        // TODO: Implement offsetExists() method.
    }

    public function offsetGet($offset)
    {
        // TODO: Implement offsetGet() method.
    }

    public function offsetSet($offset, $value)
    {
        // TODO: Implement offsetSet() method.
    }

    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
    }
}
