<?php

namespace App\Entity;

use App\Repository\SuscripcionRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SuscripcionRepository::class)
 */
class Suscripcion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_usuario;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_plan_entreno;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha_inscripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eleccion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eleccionNO;

    public function __construct()
    {
        $this->fecha_inscripcion = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUsuario(): ?int
    {
        return $this->id_usuario;
    }

    public function setIdUsuario(int $id_usuario): self
    {
        $this->id_usuario = $id_usuario;

        return $this;
    }

    public function getIdPlanEntreno(): ?int
    {
        return $this->id_plan_entreno;
    }

    public function setIdPlanEntreno(int $id_plan_entreno): self
    {
        $this->id_plan_entreno = $id_plan_entreno;

        return $this;
    }

    public function getFechaInscripcion(): ?\DateTimeInterface
    {
        return $this->fecha_inscripcion;
    }

    public function setFechaInscripcion(\DateTimeInterface $fecha_inscripcion): self
    {
        $this->fecha_inscripcion = $fecha_inscripcion;

        return $this;
    }

    public function getEleccion(): ?string
    {
        return $this->eleccion;
    }

    public function setEleccion(string $eleccion): self
    {
        $this->eleccion = $eleccion;

        return $this;
    }

    public function getEleccionNO(): ?string
    {
        return $this->eleccionNO;
    }

    public function setEleccionNO(?string $eleccionNO): self
    {
        $this->eleccionNO = $eleccionNO;

        return $this;
    }
}
