<?php

namespace App\Controller;


use App\Entity\ChangePassword;
use App\Entity\PlanEntreno;
use App\Entity\Usuario;
use App\Form\ChangePasswordType;
use App\Form\PlanEntrenoType;
use App\Repository\ComentarioRepository;
use App\Repository\PlanEntrenoRepository;
use App\Repository\SuscripcionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class PerfilUsuarioController extends AbstractController
{
    /**
     * @Route("miperfil", name="perfil_usuario")
     */
    public function miperfil()
    {
        return $this->render('miperfil.html.twig');
    }

    /**
     * @Route("mis_suscripciones", name="suscripciones_usuario")
     */
    public function mis_suscripciones(Security $security, SuscripcionRepository $sus, PlanEntrenoRepository $ruts)
    {
        $user = $security->getUser();

        $id_user = null;

        if(!is_null($user)){
            $id_user = $user->getId();
        }

        $suscripciones = $sus->findBy([
            'id_usuario' => $id_user
        ]);

        $planes = $ruts->findAll();


        //render view: mis_suscripciones
        return $this->render('mis_suscripciones.html.twig', [
            'suscripciones' => $suscripciones,
            'planes' => $planes
        ]);
    }

    /**
     * @Route("mis_rutinas", name="rutinas_usuario")
     */
    public function mis_rutinas(PlanEntrenoRepository $rutinas, Security $security)
    {
        $ruta = 'images/gallery/';
        //view: mis_rutinas
        $user = $security->getUser();

        $planes = [];

        if(!is_null($user)){
            $id_user = $user->getId();
            $planes = $rutinas->findBy(['usuarioCreador_id' => $id_user]);
        }

        $todos = [];
        if($user->getUsername() === 'admin') {
            $todos = $rutinas->findAll();
        }

        return $this->render('mis_rutinas.html.twig', ['planes' => $planes, 'user' => $user, 'ruta' => $ruta, 'todos' => $todos]);
    }

    /**
     * @param $id
     * @Route("elimina_rutina/{id}", name="elimina_rutina", requirements={"id"="\d+"})
     */
    public function elimina_rutina($id, PlanEntrenoRepository $rut, SuscripcionRepository $sus, EntityManagerInterface $entityManager)
    {

        $user = $this->getUser();
        $id_user = $user->getId();

        $rutina = $rut->find($id);
        $suscripciones = $sus->findAll();

        $id_rutinas = [];

        foreach($suscripciones as $suscripcion){
            $id_rutinas[] = $suscripcion->getIdPlanEntreno();
        }

        if (!in_array($rutina->getId(), $id_rutinas)) {
            $entityManager->remove($rutina);
            $entityManager->flush();
            return $this->redirectToRoute('perfil_usuario', [], Response::HTTP_SEE_OTHER);
        }

        return $this->redirectToRoute('hay_suscritos', [], Response::HTTP_SEE_OTHER);

    }

    /**
     * @Route("hay_suscritos", name="hay_suscritos")
     */
    public function hay_suscritos()
    {
        return $this->render('hay_suscritos.html.twig');
    }

    /**
     * @Route("actividad_usuario", name="actividad_usuario")
     */
    public function actividad_usuario(Security $security, ComentarioRepository $coms)
    {

        $user = $security->getUser();

        $comentarios = [];

        if(!is_null($user)){
            $id_user = $user->getId();
            $comentarios = $coms->findBy([
                'usuario_id' => $id_user
            ]);
        }

        //view: mis_comentarios
        return $this->render('mis_comentarios.html.twig', ['comentarios' => $comentarios, 'user' => $user]);
    }

    #[Route('/cambioPass', name: 'cambio_pass', methods: ['GET', 'POST'])]
    public function changePassword(Request $request, EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher)
    {

        $changePasswordModel = new ChangePassword();
        $form = $this->createForm(ChangePasswordType::class, $changePasswordModel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $entityManager->find(Usuario::class, $this->getUser()->getId());
            $user->setPassword(
                $passwordHasher->hashPassword(
                    $user,
                    $form->get('newPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('index_page', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('cambio_pass.html.twig', array(
            'changePasswordForm' => $form->createView(),
        ));
    }
}