<?php

namespace App\Controller;

use App\Repository\PlanEntrenoRepository;
use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @Route("/", name="index_page")
     */
    public function index(PlanEntrenoRepository $planesEnt, UsuarioRepository $us)
    {
        $ruta = 'images/gallery/';

        $planes = $planesEnt->findAll();

        $id_usuarios = [];

        foreach($planes as $plan) {
            $id_usuarios[]= $plan->getUsuarioCreadorId();
        }

        $nombre_usuarios = [];

        if(!empty($id_usuarios)){
            for($i = 0; $i<count($id_usuarios); $i++) {
                $usuarios = $us->find($id_usuarios[$i]);
                if($usuarios){
                    $nombre_usuarios[] = $usuarios->getUsername() . ' ' . $usuarios->getApellidos();
                }
            }
        }
        return $this->render('index.html.twig', [
            'planes' => $planes,
            'id_usuarios' => $id_usuarios,
            'usernames' => $nombre_usuarios,
            'ruta' => $ruta
        ]);
    }

    /**
     * @Route("about", name="about_page")
     */
    public function about()
    {
        return $this->render('about.html.twig');
    }
}