<?php

namespace App\Controller\API;

use App\BLL\PlanEntrenoBLL;
use App\Entity\PlanEntreno;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PlanEntrenoApiController extends BaseApiController
{

    /**
     * @Route("/rest_plan_entreno.{_format}", name="post_plan_entreno",
     *     defaults={"_format":"json"},
     *     requirements={"_format":"json"},
     *     methods={"POST"}
     * )
     */
    public function post(Request $request, PlanEntrenoBLL $planEntrenoBLL)
    {
        $data = $this->getContent($request);

        $plan = $planEntrenoBLL->nuevo($data);

        return $this->getResponse($plan, Response::HTTP_CREATED);
    }

    /**
     * @Route("/update_plan_entreno/{id}.{_format}", name="update_plan",
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"PUT"}
     * )
     */
    public function update(Request $request, PlanEntreno $planEntreno, PlanEntrenoBLL $planEntrenoBLL)
    {
        $data = $this->getContent($request);
        $planEntreno = $planEntrenoBLL->update($planEntreno, $data);
        return $this->getResponse($planEntreno, Response::HTTP_OK);
    }

    /**
     * @Route("/rest_plan_entreno/{id}.{_format}", name="get_plan_entreno",
     *     defaults={
     *          "_format":"json",
     *          "id":"\d+"
     *     },
     *     requirements={"_format":"json"},
     *     methods={"GET"}
     * )
     */
    public function getOne(PlanEntreno $planEntreno, PlanEntrenoBLL $planEntrenoBLL) {
        return $this->getResponse($planEntrenoBLL->toArray($planEntreno));
    }

    /**
     * @Route("/rest_planes.{_format}", name="get_planes",
     *     defaults={
     *          "_format":"json"
     *     },
     *     requirements={"_format":"json"},
     *     methods={"GET"}
     * ),
     * @Route("/rest_planes/ordenados/{order}", name="get_planes_ordenados")
     */
    public function getAll(PlanEntrenoBLL $planEntrenoBLL, Request $request, $order="ejercicio_uno") {

        $planes = $planEntrenoBLL->getContactos($order);

        return $this->getResponse($planes);
    }

    /**
     * @Route("/rest_delete_plan/{id}.{_format}", name="delete_plan_entreno",
     *     defaults={
     *          "_format":"json",
     *          "id":"\d+"
     *     },
     *     requirements={"_format":"json"},
     *     methods={"DELETE"}
     * )
     */
    public function delete(PlanEntreno $planEntreno, PlanEntrenoBLL $planEntrenoBLL) {

        $planEntrenoBLL->delete($planEntreno);

        return $this->getResponse(null, Response::HTTP_NO_CONTENT);
    }
}