<?php

namespace App\Controller;

use App\Entity\Mensajes;
use App\Form\MensajesType;
use App\Repository\MensajesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/mensajes')]
class MensajesController extends AbstractController
{
    #[Route('/', name: 'mensajes_index', methods: ['GET'])]
    public function index(MensajesRepository $mensajesRepository): Response
    {
        return $this->render('mensajes/index.html.twig', [
            'mensajes' => $mensajesRepository->findAll(),
        ]);
    }

    #[Route('-contact', name: 'mensajes_contact', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $mensaje = new Mensajes();
        $form = $this->createForm(MensajesType::class, $mensaje);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($mensaje);
            $entityManager->flush();

            return $this->redirectToRoute('mensajes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('mensajes/new.html.twig', [
            'mensaje' => $mensaje,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'mensajes_show', methods: ['GET'])]
    public function show(Mensajes $mensaje): Response
    {
        return $this->render('mensajes/show.html.twig', [
            'mensaje' => $mensaje,
        ]);
    }

    #[Route('/{id}/edit', name: 'mensajes_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Mensajes $mensaje, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(MensajesType::class, $mensaje);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('mensajes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('mensajes/edit.html.twig', [
            'mensaje' => $mensaje,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'mensajes_delete', methods: ['POST'])]
    public function delete(Request $request, Mensajes $mensaje, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$mensaje->getId(), $request->request->get('_token'))) {
            $entityManager->remove($mensaje);
            $entityManager->flush();
        }

        return $this->redirectToRoute('mensajes_index', [], Response::HTTP_SEE_OTHER);
    }
}
