<?php

namespace App\Controller;

use App\BLL\PlanEntrenoBLL;
use App\Entity\PlanEntreno;
use App\Form\PlanEntrenoEditType;
use App\Form\PlanEntrenoType;
use App\Repository\PlanEntrenoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/plan_entreno')]
class PlanEntrenoController extends AbstractController
{
    #[Route('/', name: 'plan_entreno_index', methods: ['GET'])]
    public function index(PlanEntrenoRepository $planEntrenoRepository): Response
    {
        return $this->render('plan_entreno/index.html.twig', [
            'plan_entrenos' => $planEntrenoRepository->findAll(),
        ]);
    }

    #[Route('-crear_rutina', name: 'crear_rutina', methods: ['GET', 'POST'])]
    public function new(Request $request, PlanEntrenoBLL $planEntrenoBLL): Response
    {
        $planEntreno = new PlanEntreno();
        $form = $this->createForm(PlanEntrenoType::class, $planEntreno);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $planEntrenoBLL->addEntreno($form, $planEntreno);
            return $this->redirectToRoute('perfil_usuario', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('plan_entreno/new.html.twig', [
            'plan_entreno' => $planEntreno,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'plan_entreno_show', methods: ['GET'])]
    public function show(PlanEntreno $planEntreno): Response
    {
        return $this->render('plan_entreno/show.html.twig', [
            'plan_entreno' => $planEntreno,
        ]);
    }

    #[Route('/{id}/edit', name: 'plan_entreno_edit', methods: ['GET', 'POST'])]
    public function edit($id, Request $request, PlanEntreno $planEntreno, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(PlanEntrenoEditType::class, $planEntreno);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $img = $form['imagen']->getData();

            $planEntreno->setImagen($img);

            $entityManager->flush();

            return $this->redirectToRoute('detalles_rutina', ['id' => $id], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('plan_entreno/edit.html.twig', [
            'plan_entreno' => $planEntreno,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'plan_entreno_delete', methods: ['POST'])]
    public function delete(Request $request, PlanEntreno $planEntreno, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$planEntreno->getId(), $request->request->get('_token'))) {
            $entityManager->remove($planEntreno);
            $entityManager->flush();
        }

        return $this->redirectToRoute('plan_entreno_index', [], Response::HTTP_SEE_OTHER);
    }
}
