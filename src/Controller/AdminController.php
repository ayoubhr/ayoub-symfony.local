<?php

namespace App\Controller;

use App\Repository\ComentarioRepository;
use App\Repository\PlanEntrenoRepository;
use App\Repository\SuscripcionRepository;
use App\Repository\UsuarioRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdminController extends AbstractController
{
    /**
     * @Route("administrador", name="ruta_admin")
     */
    public function administrador(UsuarioRepository $usr)
    {
        $usuarios = $usr ->findAll();

        return $this->render('administrador.html.twig', ['usuarios' => $usuarios]);
    }

    /**
     * @param $id
     * @Route("info_page/{id}", name="info_page_user", requirements={"id"="\d+"})
     */
    public function info_page($id, UsuarioRepository $usr, SuscripcionRepository $subs, ComentarioRepository $coms, PlanEntrenoRepository $ruts)
    {
        $usuario = $usr->find($id);

        $suscripciones = $subs->findBy([
            'id_usuario' => $id
        ]);

        $comentarios = $coms->findBy([
            'usuario_id' => $id
        ]);

        $rutinas = $ruts->findBy([
            'usuarioCreador_id' => $id
        ]);

        return $this->render('info_page.html.twig', [
            'usuario' => $usuario,
            'suscripciones' => $suscripciones,
            'comentarios' => $comentarios,
            'rutinas' => $rutinas
        ]);
    }

    /**
     *
     * @Route("info_page/delete_user/{id}", name="delete_user", requirements={"id"="\d+"})
     */
    public function delete_user($id, UsuarioRepository $usr, EntityManagerInterface $entityManager, PlanEntrenoRepository $pe)
    {
        $usuario = $usr->find($id);

        $num = $pe->findBy([
            'usuarioCreador_id' => $id
        ]);

        if(count($num) >= 1){
            return $this->render('error.html.twig', []);
        } else {
            $entityManager->remove($usuario);
            $entityManager->flush();

            return $this->redirectToRoute('ruta_admin', [], Response::HTTP_SEE_OTHER);
        }
    }
}