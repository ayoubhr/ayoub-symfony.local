<?php

namespace App\Controller;

use App\Entity\Suscripcion;
use App\Repository\ComentarioRepository;
use App\Repository\PlanEntrenoRepository;
use App\Repository\SuscripcionRepository;
use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class RutinaController extends AbstractController
{
    /**
     * @param $id
     * @Route("detalles_rutina/{id}", name="detalles_rutina", requirements={"id"="\d+"})
     */
    public function detalles_rutina($id, Security $security, PlanEntrenoRepository $rutinas, UsuarioRepository $usuarios, ComentarioRepository $coms, SuscripcionRepository $sus){

        $ruta = 'images/gallery/';

        $rutina = $rutinas->find($id);

        $usuario = $usuarios->findBy(
           ['id' => $rutina->getUsuarioCreadorId()]
        );

        $comentarios = $coms->findBy(
            ['rutina_id' => $id]
        );

        $suscripciones = $sus->findBy([
            'id_plan_entreno' => $id
        ]);

        $user = $security->getUser();

        $suscrito = null;

        if(!is_null($user)){
            $id_user = $user->getId();
            $suscrito = $sus->findBy([
                'id_usuario' => $id_user,
                'id_plan_entreno' => $id
            ]);
        }

        $lo_esta = 1;

        if(!is_null($suscrito)){
            $lo_esta = 2;
        } else {
            $lo_esta = 0;
        }

        $usuarios_comentadores = [];

        foreach ($comentarios as $comentario) {
            $id_comentarista = $comentario->getUsuarioId();
            $comentarista = $usuarios->find($id_comentarista);
            $usuarios_comentadores[] = $comentarista->getUsername();
        }

        //render view detalles_rutina + data adecuada
        return $this->render('detalles_rutina.html.twig', [
            'rutina' => $rutina,
            'usuario' => $usuario,
            'comentarios' => $comentarios,
            'usuarios_comentadores' => $usuarios_comentadores,
            'suscripciones' => $suscripciones,
            'lo_esta' => $lo_esta,
            'user' => $user,
            'ruta' => $ruta
        ]);
    }
}