<?php

namespace App\BLL;

use App\Entity\PlanEntreno;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PlanEntrenoBLL extends BaseBLL
{

    public function addEntreno(FormInterface $form, PlanEntreno $planEntreno)
    {
        //Campo donde se guarda el el objeto Uploaded file
        /** @var UploadedFile $file */
        $file = $form['imagen']->getData();

        //Genera un nombre unico para el fichero antes de guardarlo
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();

        //Mueve el fichero a la carpeta destino donde se va a guardar
        $file->move(
            $this->getParameter('images_directory'),
            $fileName
        );

        //actualiza la propiedad imagen en el objeto PlanEntreno para que guarde el nombre de la imagen en lugar de su contenido
        $planEntreno->setImagen($fileName);

        //Get creator user id
        $user = $this->getUser();
        $id = $user->getId();

        $planEntreno->setUsuarioCreadorId($id);

        $this->em->persist($planEntreno);
        $this->em->flush();
    }

    public function nuevo(array $data)
    {
        $plan = new PlanEntreno();

        $plan->setImagen($data['imagen']);
        $plan->setNombre($data['nombre']);

        $plan->setEjercicioUno($data['ejercicio_uno']);
        $plan->setEjercicioDos($data['ejercicio_dos']);
        $plan->setEjercicioTres($data['ejercicio_tres']);
        $plan->setEjercicioCuatro($data['ejercicio_cuatro']);
        $plan->setEjercicioCinco($data['ejercicio_cinco']);

        $plan->setUsuarioCreadorId(9);
        $plan->setDescripcion($data['descripcion']);
        $plan->setTipo($data['tipo']);

        return $this->guardaValidando($plan);
    }

    public function getContactos(string $order) {
        $planes = $this->em->getRepository(PlanEntreno::class)->findBy([], [$order => 'ASC']);

        return $this->entitiesToArray($planes);
    }

    public function update(PlanEntreno $plan, $data)
    {
        $plan->setImagen($data['imagen']);
        $plan->setNombre($data['nombre']);

        $plan->setEjercicioUno($data['ejercicio_uno']);
        $plan->setEjercicioDos($data['ejercicio_dos']);
        $plan->setEjercicioTres($data['ejercicio_tres']);
        $plan->setEjercicioCuatro($data['ejercicio_cuatro']);
        $plan->setEjercicioCinco($data['ejercicio_cinco']);

        $plan->setUsuarioCreadorId(9);
        $plan->setDescripcion($data['descripcion']);
        $plan->setTipo($data['tipo']);

        return $this->guardaValidando($plan);
    }

    public function toArray($plan){
        if(is_null($plan)){
            return null;
        }

        if(!($plan instanceof PlanEntreno)){
            throw new Exception("El plan de entreno no cumple con el formato");
        }

        return [
            'id' => $plan->getId(),
            'imagen' => $plan->getImagen(),
            'nombre' => $plan->getNombre(),
            'ejercicio_uno' => $plan->getEjercicioUno(),
            'ejercicio_dos' => $plan->getEjercicioDos(),
            'ejercicio_tres' => $plan->getEjercicioTres(),
            'ejercicio_cuatro' => $plan->getEjercicioCuatro(),
            'ejercicio_cinco' => $plan->getEjercicioCinco(),
            'descripcion' => $plan->getDescripcion(),
            'usuarioCreador_id' => $plan->getUsuarioCreadorId(),
            'tipo' => $plan->getTipo()
        ];
    }
}