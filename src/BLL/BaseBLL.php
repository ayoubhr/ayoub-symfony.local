<?php

namespace App\BLL;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class BaseBLL extends AbstractController
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var ValidatorInterface */
    protected $validator;

    /**
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    private function validate($entity) {
        $errors = $this->validator->validate($entity);

        if(count($errors) > 0){
            $strError = "";
            foreach ($errors as $error){
                if(!empty($strError)){
                    $strError .= '\n';
                }
                $strError .= $error->getMessage();
            }
            throw new BadRequestHttpException($strError);
        }
    }

    protected function guardaValidando($entity) {

        $this->validate($entity);

        $this->em->persist($entity);
        $this->em->flush();

        return $this->toArray($entity);
    }

    public function entitiesToArray(array $entities){
        if(is_null($entities)){
            return null;
        }
        $arr = [];

        foreach ($entities as $entity){
            $arr[] = $this->toArray($entity);
        }

        return $arr;
    }

    public function delete($entity){
        $this->em->remove($entity);
        $this->em->flush();
    }

    abstract public function toArray($entity);

}