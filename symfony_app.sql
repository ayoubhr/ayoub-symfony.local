-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 13-02-2022 a las 03:03:21
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `symfony_app`
--
CREATE DATABASE IF NOT EXISTS `symfony_app` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `symfony_app`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `texto` longtext COLLATE utf8_spanish_ci NOT NULL,
  `rutina_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`id`, `usuario_id`, `texto`, `rutina_id`, `date`) VALUES
(7, 18, 'Muy top la rutina Ayoub, la intentaré seguir!!', 15, '2022-02-13 02:05:20'),
(8, 19, 'SIento bastante curiosidad por esta rutina, me la apunto!\r\n\r\nUn saludo.', 15, '2022-02-13 02:09:08'),
(9, 18, 'Enorme Maria, tus rutinas van mejorando!!!', 19, '2022-02-13 02:45:16'),
(10, 17, 'Muchas gracias chicos, se vienen cosas!!!!!!!!!', 15, '2022-02-13 02:46:09'),
(11, 17, 'Voy a ver si quemo las piernas con tu rutina Adri jajajaj', 17, '2022-02-13 02:46:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20211222204222', '2021-12-22 21:42:54', 157),
('DoctrineMigrations\\Version20211223202956', '2021-12-23 21:30:25', 98),
('DoctrineMigrations\\Version20211223204341', '2021-12-23 21:43:48', 107),
('DoctrineMigrations\\Version20211226010301', '2021-12-26 02:03:16', 34),
('DoctrineMigrations\\Version20211226013039', '2021-12-26 02:30:56', 51),
('DoctrineMigrations\\Version20211226171600', '2021-12-26 18:16:13', 126),
('DoctrineMigrations\\Version20211226172349', '2021-12-26 18:23:54', 99),
('DoctrineMigrations\\Version20211226184713', '2021-12-26 19:47:18', 121),
('DoctrineMigrations\\Version20211226185503', '2021-12-26 19:55:07', 113),
('DoctrineMigrations\\Version20220212012701', '2022-02-12 02:27:23', 120);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `mensaje` longtext COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_entreno`
--

CREATE TABLE `plan_entreno` (
  `id` int(11) NOT NULL,
  `imagen` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `ejercicio_uno` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `ejercicio_dos` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `ejercicio_tres` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `ejercicio_cuatro` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `ejercicio_cinco` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` longtext COLLATE utf8_spanish_ci NOT NULL,
  `usuario_creador_id` int(11) DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `plan_entreno`
--

INSERT INTO `plan_entreno` (`id`, `imagen`, `nombre`, `ejercicio_uno`, `ejercicio_dos`, `ejercicio_tres`, `ejercicio_cuatro`, `ejercicio_cinco`, `descripcion`, `usuario_creador_id`, `tipo`) VALUES
(15, '73abac1a7bce26fe5709d91a64e369bf.jpg', 'FullPlanche', 'Flexiones', 'Leg lifts', 'Squats', 'Russian abs', 'Pull Ups', 'Flexiones 4x15, 1\' de descanso entre series.\r\nLeg lifts 4x15, 30 segundos de descanso entre series.\r\nSquats 1x50.\r\nRussian Abs 3x10 cada lado, 1\' de descanso entre series.\r\nPull Ups 5x10.', 17, 'Push'),
(16, '39b5ffbb3017cfaeaf2ae7cec634e5e7.png', 'BackLever', 'PullUps', 'Skin the cat', 'Leg lifts', 'Lumbares', 'Squats', 'Pull Ups 5x10, 2 minutos de descanso entre series.\r\nSkin the cat, 5 repeticiones después de cada serie de dominadas.\r\nLeg lifts 4x15, 30 segundos de descanso entre series.\r\nLumbares 3x30, 2 minutos de descanso entre series.\r\nSquats 1x100.', 17, 'Pull'),
(17, '1daa2d82c8e9a0e7e4fc4722ac9a84d8.png', 'Pistol Squat', 'Pistol', 'Sentadillas', 'Extensiones', 'Zancadas', 'Abs', 'Pistol 4x15 cada pierna, 3 minutos de descanso entre cada serie.\r\nSentadillas 4x20, 2 minutos de descanso entre cada serie.\r\nExtensiones 3x10, 1 minuto de descanso entre cada serie.\r\nZancadas 1x30 cada pierna.\r\nAbs 4x15, 30 segundos de descanso entre cada serie.', 18, 'Legs'),
(18, '14c8f57d8f5d9e983aa8cf0d33ee043b.jpg', 'Handstand', 'Wall Hold', 'Hollow body', 'Dragon flag', 'Leg lift', 'Pull Ups', 'Wall hold 4x1\', 5 minutos de descanso entre cada hold.\r\nHollow body 5x30\'\', 1 minuto de descanso entre cada hold.\r\nDragon flag, 3x15, 1 minuto de descanso entre cada serie.\r\nLeg lift 4x15, 1 minuto de descanso entre cada serie.\r\nPull ups 1x30.', 18, 'Push'),
(19, '634a9d2a06fe3dade0e81a1fc92441ab.jpg', 'Glute workout', 'Zancadas', 'Sentadillas', 'Extensiones', 'Lumbares', 'Hip thrust', 'Zancadas 4x20 cada pierna, 3 minutos de descanso entre cada serie.\r\nSentadillas 4x20, 3 minutos de descanso entre cada serie.\r\nExtensiones 4x20, 3 minutos de descanso entre cada serie.\r\nLumbares 2x30, 5 minutos de descanso entre cada serie.\r\nHip thrust 4x10 +100kg, 3 minutos de descanso entre cada serie.', 19, 'Legs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `suscripcion`
--

CREATE TABLE `suscripcion` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_plan_entreno` int(11) NOT NULL,
  `fecha_inscripcion` datetime NOT NULL,
  `eleccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `eleccion_no` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `suscripcion`
--

INSERT INTO `suscripcion` (`id`, `id_usuario`, `id_plan_entreno`, `fecha_inscripcion`, `eleccion`, `eleccion_no`) VALUES
(10, 18, 15, '2022-02-13 02:04:57', NULL, NULL),
(11, 19, 15, '2022-02-13 02:09:22', NULL, NULL),
(12, 18, 19, '2022-02-13 02:44:54', NULL, NULL),
(13, 17, 17, '2022-02-13 02:46:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_alta` date DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `roles` longtext COLLATE utf8_spanish_ci NOT NULL COMMENT '(DC2Type:json)',
  `is_verified` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `email`, `password`, `username`, `apellidos`, `fecha_alta`, `telefono`, `direccion`, `roles`, `is_verified`) VALUES
(9, 'admin@admin.com', '$2y$13$nc0z2hWwtHj/ULnvt.Q0mehrTS638OdWOUWkwKdJ5aD2/wE7zl5Oi', 'admin', 'admin', '2021-12-26', 123456789, 'avd admin', '[\"ROLE_ADMIN\"]', 0),
(17, 'usuario_uno@gmail.com', '$2y$13$53D7DZjdVsfSEI20RQ52mO5hHPDCpj1hTLBnOIXLUZVbmUxK0Payy', 'Ayoub', 'Hiar', '2022-02-13', 67676767, 'Calle cale', '[\"ROLE_USER\"]', 0),
(18, 'usuario_dos@gmail.com', '$2y$13$Rw3eN0CYNmah0pJZvuWUuOPl5T.jaHi6kicw0AiqTq08wlf98SBt.', 'Adrian', 'Garcia', '2022-02-13', 65656565, 'Avenida avd', '[\"ROLE_USER\"]', 0),
(19, 'usuario_tres@hotmail.com', '$2y$13$HA7HSzfgz.a8gGygh9VuWOiQIAXaltg8drAmEML9XnggAnOVbGwTe', 'Maria', 'Ruiz', '2022-02-13', 62626262, 'Calle avd', '[\"ROLE_USER\"]', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `plan_entreno`
--
ALTER TABLE `plan_entreno`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_2265B05DE7927C74F85E0677` (`email`,`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `plan_entreno`
--
ALTER TABLE `plan_entreno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
